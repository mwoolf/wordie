//
//  Guess.swift
//  wordy
//
//  Created by Max Woolf on 11/01/2022.
//

import Foundation

struct Guess {
    let word: Word
    let submittedGuess: String

    var letters: [String] {
        get {
            self.submittedGuess.map { String($0).uppercased() }
        }
    }
    
    var isValid: Bool {
        get {
            word.letters == self.letters
        }
    }
    
    var result: [LetterStatus] {
        get {
            var result = [LetterStatus]()
            
            for(letter, wordLetter) in zip(self.letters, word.letters) {
                if(letter == wordLetter) {
                    result.append(.correct)
                } else if(word.letters.contains(letter)) {
                    result.append(.wrongLocation)
                } else {
                    result.append(.nonExistant)
                }
            }
            
            return result
        }
    }
}
